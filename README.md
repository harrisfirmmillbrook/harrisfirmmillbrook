The Harris Firm LLC - Divorce Lawyer and Bankruptcy Attorney in Millbrook, Alabama

The Harris Firm is a law firm that helps individuals throughout Millbrook, AL in the areas of bankruptcy, family law, divorce, probate, and injury. Many of these cases are worked on a retainer basis or contingency fee.

Address: 2161 Main St, Millbrook, AL 36054, USA

Phone: 334-452-4899

Website: https://www.theharrisfirmllc.com/millbrook-divorce
